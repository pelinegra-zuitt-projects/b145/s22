// Syntax:
// {
// 	"propertyA": "valueA"	
// 	"propertyB": "valueB"
// }

// {
// 	"city": "Quezon City",
// 	"province":"Metro Manila",
// 	"country":"Philippines"
// }

//Javascript array of objects
// let myArr = [
// 	{name: "Jino"},
// 	{name: "John"}
// ]

//Array of JSON objects
// {
// 	"cities": [
// 		{"city": "Quezon City"},
// 		{"city": "Makati City"}
// 	]

// }

//in JSON, number and boolean values = no "" 

//Converting JS data into stringified JSON

//Stringified JSON is a Javascript Object converted into a string to be used by the receiving backend application or other functions of a JavaScript application

let batchesArr = [
	{batchname: 'Batch 145'},
	{batchname: 'Batch 146'},
	{batchname: 'Batch 147'},
]

let stringifiedData = JSON.stringify(batchesArr)
console.log(stringifiedData)

let fixedData = JSON.parse(stringifiedData)
console.log(fixedData)